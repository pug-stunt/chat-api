
module.exports = function(db, passport) {

    var config              = require('./config')
      , express             = require('express')
      , cookieParser        = require('cookie-parser')
      , session             = require('express-session')
      , passport            = require('passport')
      , bodyParser          = require('body-parser')


    require('./passport')(passport)
    var app = express()

    app.use(cookieParser())
    app.use(session({
        saveUninitialized: false,
        resave: false,
        secret: config.sessionSecret
    }))

    app.use(bodyParser.urlencoded({ extended: true }))
    app.use(bodyParser.json())
    app.set('views', __dirname + '/../app/views')
    app.use(passport.initialize())
    app.use(passport.session())

    require('../app/routes/chat.server.routes.js')(app)
    require('../app/routes/user.server.routes.js')(app)
    require('../app/routes/auth.server.routes.js')(app, passport)

    app.use(express.static('public'))

    return app
}
