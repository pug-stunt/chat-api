module.exports = {
  db: '',
  sessionSecret: '',
  auth: {
    google: {
      clientID: '',
      clientSecret: '',
      callbackURL: 'http://localhost:5000/auth/google/callback'
    }
  }
}
