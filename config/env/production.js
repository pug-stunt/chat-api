module.exports = {
  db : process.env.MONGODB,
  sessionSecret : process.env.SESSION_SECRET,
  auth: {
    google: {
      clientID: process.env.AUTH_GOOGLE_CLIENT_ID,
      clientSecret: process.env.AUTH_GOOGLE_CLIENT_SECRET,
      callbackURL: process.env.AUTH_GOOGLE_CALLBACK_URL
    }
  }
}
