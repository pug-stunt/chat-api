var config          = require('./config')
var User            = require('mongoose').model('User')
var GoogleStrategy  = require('passport-google-oauth').OAuth2Strategy;

module.exports = function(passport) {

  passport.serializeUser(function(user, done) {
    done(null, user.id)
  })

  passport.deserializeUser(function(id, done) {
    User.findOne({id: id}, function(err, user) {
      done(err, user)
    })
  })

  passport.use(new GoogleStrategy({
    clientID: config.auth.google.clientID,
    clientSecret: config.auth.google.clientSecret,
    callbackURL: config.auth.google.callbackURL
  },
  function(accessToken, refreshToken, profile, done) {
    process.nextTick(function() {

      User.findOne({ id: profile.id }, function(err, user) {

        if (err) {
          return done(err)
        }

        if (user) {
          return done(null, user)
        } else {

          var newUser = new User()
          newUser.id = profile.id
          newUser.token = accessToken
          newUser.name = profile.displayName
          newUser.email = profile.emails[0].value

          newUser.save(function(err) {
            if (err) {
              throw err
            }
            return done(null, newUser)
          })

        }

      })
    })
  }))

  return passport
}
