module.exports = function(app, passport) {

  app.get('/auth/google',
    passport.authenticate('google', { scope: [ 'profile', 'email' ]})
  )

  app.get('/auth/google/callback',
    passport.authenticate('google', {
      successRedirect: '/auth/authorized',
      failureRedirect: '/auth/unauthorized'
    }
  ))

  app.get('/auth/authorized', function(request, response) {

    var info = {
      username: request.user.name,
      email: request.user.email
    }
    response.status(200).send('Access Authorized to ' + JSON.stringify(info))
  })

  app.get('/auth/unauthorized', function(request, response) {
    response.status(401).send('Access Unauthorized')
  })

}
