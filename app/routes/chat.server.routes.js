module.exports = function(app) {

    var chat = require('../controllers/chat.server.controller.js')

    app.route('/chat/*').get(chat.checkSession)
    app.route('/chat').get(chat.checkSession, chat.open)

    app.param('userId', chat.history)
    app.get('/chat/history/:userId', chat.history)

}
