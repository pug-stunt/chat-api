var path = require('path')

exports.checkSession = function(request, response, next) {
  if (!request.user) {
    return response.status(401).send('Unauthorized')
  }
  next()
}

exports.open = function(request, response, next) {
  response.sendFile(path.resolve(__dirname + '/../views/index.html'))
}

exports.history = function(request, response, next, userId) {

    console.log('Retrieving history of userID=' + userId)

    var mock = {
	    name: "Fernando Falci",
		    messages: [
		    {
			    date: new Date(),
			    text: "Olar",
			    sender: {
				    name: "Fernando Falci",
				    email: "falci@falci.me"
			    }
		    },
		    {
			    date: new Date(),
			    text: "Test",
			    sender: {
				    name: "Fernando Falci",
				    email: "falci@falci.me"
			    }
		    }
	    ]
    }

    response.send(mock)
}
