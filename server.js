process.env.NODE_ENV        = process.env.NODE_ENV || 'development'
const port                  = process.env.PORT || 5000
const db                    = require('./config/mongoose')
const express               = require('./config/express')
const app                   = express(db())

const server  = app.listen(port, () => { console.log('listening on ' + port) })
const io      = require('socket.io')(server)

io.on('connection', (socket) => {
    console.log('a user connected')

    socket.on('message', (message) => {
        console.log(message)
        io.emit('message', message)
    })

    socket.on('disconnect', () => {
        console.log('user disconnected')
    })

})

module.exports = app
